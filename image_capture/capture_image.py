#!/usr/bin/env python

import cv2
import sys
import numpy as np

def print_help():
    print '''
    Usage:

    capture_image.py [output_filename]

    NOTE: PLEASE remember to append .jpg onto the end of your filename.
    '''

# Read arguement of image name
if len(sys.argv) != 2:
    print("Error! Incorrect input arguements")
    print_help()
    exit(1)
else:
    name = sys.argv[1]

# Image capture
cap = cv2.VideoCapture(0)
counter = 0;

while(True):
    if counter > 10:
        break

    # Capture frame-by-frame
    ret, frame = cap.read()

    counter=counter + 1

# When image capture done, release the capture
cap.release()
cv2.destroyAllWindows()

cv2.imwrite(name,frame)
