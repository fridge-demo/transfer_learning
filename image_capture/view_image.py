#!/usr/bin/env python

import os
import cv2
import sys
import numpy as np

def print_help():
    print '''
    Usage:

    capture_image.py [input_filename/path]
    '''

# Read arguement of image name
if len(sys.argv) != 2:
    print("Error! Incorrect input arguements")
    print_help()
else:
    file_name = sys.argv[1]

# Check if file exists
if os.path.exists(file_name) != True:
    print("Error! Cannot find file")
    exit(1)
elif os.stat(file_name).st_size == 0:
    print("Error! Invalid file")
    exit(1)

img = cv2.imread(file_name)
IMG_WIDTH = 1024
IMG_HEIGHT= 656
image = cv2.resize(img, (IMG_WIDTH,IMG_HEIGHT))
#image = cv2.imread(file_name, IMREAD_COLOR)
cv2.namedWindow("Display Window")

while(True):
    cv2.imshow("Display Window", image)
    #cv2.namedWindow("Display Window", WINDOW_AUTOSIZE)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
