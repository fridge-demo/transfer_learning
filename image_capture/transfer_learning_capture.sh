#!/bin/bash

ROOT_DIR="/home/root/tl_images"

if [ -z $1 ];then
	echo "Error running script. Please enter product type as \$1"
	exit 1
fi

CATEGORY_NAME="$1"
TARGET_DIR="${ROOT_DIR}"/"${CATEGORY_NAME}"

if [ ! -d "${TARGET_DIR}" ];then
	mkdir -p "${TARGET_DIR}"
fi

# If file exists, don't overwrite file. Instead start enumeration at the next
# power of 10.
j=0
i=0
while true;do
	file="${TARGET_DIRECTORY}"/"${CATEGORY_NAME}$i".jpg
	echo "Initial filepath=${file}"
	if [ -f "${file}" ];then
		j=$j+1
		i=$(( 10 ** j ))
	else
		break
	fi
done

user_input=""

while [ "${user_input}" != "q" ];do
	./capture_image.py "${TARGET_DIR}"/"${CATEGORY_NAME}""$i".jpg
	read -p "Please click to continue. Enter \"q\" to quit. " user_input
	i=$(expr $i + 1)
done
