# Object Detection Transfer Learning

The notebook runs you through transfer learning. The best way to run the
notebook is to import it in Google Colab. You will need a Google account to run
it.

## Creating the Dataset and uploading to Google

1) Take photos of the items you would like the model to identify (min. 30 photos
   per item).
2) Split the images into training (2/3 of the images) and validation (1/3 of the
   images.
3) Install labelImg.
```
sudo apt-get install pyqt5-dev-tools
sudo pip3 install -r requirements/requirements-linux-python3.txt
make qt5py3
python3 labelImg.py
```
4) When labelImg is open, click on the "Open\_Dir" tab on the left hand side and
   open your training images directory.
5) Create a bounding box round the item(s) in the first image. Once you have
   done this, a pop up text box will appear. Write the label name for the object
   in the text box. (NOTE!! Make this label consistent across all the images,
   both training and validation).
6) SAVE the label.
7) Move onto the next image and do the same.
8) Once you have completed labeling for all the training images, do steps 5-7
   for the validation images.
9) Copy the training and validation folders to an appropriate location on your
   Google Drive account. These folders should have both the images and their
   corresponding xml label files.

## Using the notebook

1) Download the notebook OR create a Github mirror of the repository.
2) Head to [Google Colab](https://colab.research.google.com/notebooks/intro.ipynb#recent=true)
3) If you have downloaded the notebook, upload it to Google Colab.
   If you have mirrored the repo in Github, head to the GitHub tab and add the
   link to the file.

--------------------------------------------------------------------------------
This project is licensed under the terms of the MIT license (please see
COPYING.MIT for further details).
